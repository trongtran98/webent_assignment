/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entities.Comment;
import entities.Post;
import entities.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import trongtran.ConnectDB;

/**
 *
 * @author Trong Tran
 */
public class ManagerDataAccess {

    private final String USER = "sa";
    private final String PASS = "123456";
    private final String DATABASE_NAME = "WEBENT_ASSIGNMENT";
    ConnectDB connectDB = new ConnectDB();

    public boolean addUser(User user) {
        try {
            String query = "  INSERT INTO dbo.dboUSER( account ,password ,avatar ,name ,location ,age ,joined ,introduction ,regency)"
                    + "VALUES(?,?,?,?,?,?,GETDATE(),?,'USER')";
            Connection connection = connectDB.getConnectionMSSQL(USER, PASS, DATABASE_NAME);
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, user.getAccount());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getAvatar());
            ps.setString(4, user.getName());
            ps.setString(5, user.getLocation());
            ps.setInt(6, user.getAge());
            ps.setString(7, user.getIntroduction());
            int result = ps.executeUpdate();
            return result > 0;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public boolean addPost(Post post) {
        try {
            String query = "INSERT INTO dboPOST(user_account,content,picture) VALUES(?,?,?)";
            Connection connection = connectDB.getConnectionMSSQL(USER, PASS, DATABASE_NAME);
            PreparedStatement ps = connection.prepareStatement(query);

            ps.setString(1, post.getUser_account());
            ps.setString(2, post.getContent());
            ps.setString(3, post.getPicture());
            int result = ps.executeUpdate();
            return result > 0;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public boolean addComment(Comment comment) {
        try {
            String query = "INSERT INTO dboCOMMENT(user_account,post_id,content,picture) VALUES(?,?,?,?)";
            Connection connection = connectDB.getConnectionMSSQL(USER, PASS, DATABASE_NAME);
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, comment.getUser_account());
            ps.setInt(2, comment.getPost_id());
            ps.setString(3, comment.getContent());
            ps.setString(4, comment.getPicture());
            int result = ps.executeUpdate();
            return result > 0;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public String login(User user) {
        try {
            String query = "SELECT * FROM dboUSER WHERE account = ? AND password = ?";
            Connection connection = connectDB.getConnectionMSSQL(USER, PASS, DATABASE_NAME);
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, user.getAccount());
            ps.setString(2, user.getPassword());
            ResultSet rs = ps.executeQuery();
            String regency = null;
            while (rs.next()) {
                regency = rs.getString("regency");
            }
            return regency;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public List<Post> getUserPosts() {
        List<Post> posts = new LinkedList<>();
        try {
            String query = "SELECT * FROM dboPOST ORDER BY id DESC";
            Connection connection = connectDB.getConnectionMSSQL(USER, PASS, DATABASE_NAME);
            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                posts.add(new Post(rs.getInt("id"), rs.getString("user_account"), rs.getString("content"), rs.getString("picture"), rs.getTimestamp("create")));
            }
            
            return posts;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public List<Comment> getCommentsPostID(String post_id) {
        List<Comment> comments = new LinkedList<>();
        try {
            String query = "SELECT * FROM dboCOMMENT WHERE post_id = ? ORDER BY id DESC";
            Connection connection = connectDB.getConnectionMSSQL(USER, PASS, DATABASE_NAME);
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, post_id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                comments.add(new Comment(rs.getInt("id"),rs.getString("user_account"),rs.getInt("post_id"), rs.getString("content"), rs.getString("picture"), rs.getTimestamp("created")));
            }
            return comments;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public User getUserByAcc(String acc) {
        User user = null;
        try {
            String query = "SELECT * FROM dboUSER WHERE account = ?";
            Connection connection = connectDB.getConnectionMSSQL(USER, PASS, DATABASE_NAME);
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, acc);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                user = new User(rs.getString("name"), rs.getString("account"), null, rs.getString("avatar"), rs.getString("location"), rs.getInt("age"), rs.getTimestamp("joined"), rs.getString("introduction"), rs.getString("regency"));
            }
            return user;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public boolean updateUser(User user) {
        try {
            String query = "UPDATE dboUSER SET avatar = ?, name = ?, location = ?, age = ?, introduction = ? WHERE account = ?";
            Connection connection = connectDB.getConnectionMSSQL(USER, PASS, DATABASE_NAME);
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, user.getAvatar());
            ps.setString(2, user.getName());
            ps.setString(3, user.getLocation());
            ps.setInt(4, user.getAge());
            ps.setString(5, user.getIntroduction());
            ps.setString(6, user.getAccount());
            int result = ps.executeUpdate();
            return result > 0;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public String checkAdmin(String acc) {
        try {
            String query = "SELECT regency FROM dboUSER WHERE account = ?";
            Connection connection = connectDB.getConnectionMSSQL(USER, PASS, DATABASE_NAME);
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, acc);
            ResultSet rs = ps.executeQuery();
            String regency = null;
            while (rs.next()) {
                regency = rs.getString("regency");
            }
            return regency;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

}
