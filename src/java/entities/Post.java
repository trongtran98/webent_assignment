/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.sql.Timestamp;

/**
 *
 * @author Trong Tran
 */
public class Post {
    private int id;
    private String user_account;
    private String content;
    private String picture;
    private Timestamp create;
    
    public Post() {
    }

    public Post(int id, String user_account, String content, String picture, Timestamp create) {
        this.id = id;
        this.user_account = user_account;
        this.content = content;
        this.picture = picture;
        this.create = create;
    }

    
    
    public String getUser_account() {
        return user_account;
    }

    public void setUser_account(String user_account) {
        this.user_account = user_account;
    }

    

    public Timestamp getCreate() {
        return create;
    }

    public void setCreate(Timestamp create) {
        this.create = create;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
    
    
}
