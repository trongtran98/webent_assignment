/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.sql.Timestamp;

/**
 *
 * @author Trong Tran
 */
public class Comment {
    private int id;
    private String user_account;
    private int post_id;
    private String content;
    private String picture;
    private Timestamp create;
    
    public Comment() {
    }

    public Comment(int id, String user_account, int post_id, String content, String picture, Timestamp create) {
        this.id = id;
        this.user_account = user_account;
        this.post_id = post_id;
        this.content = content;
        this.picture = picture;
        this.create = create;
    }

    public String getUser_account() {
        return user_account;
    }

    public void setUser_account(String user_account) {
        this.user_account = user_account;
    }

    public int getPost_id() {
        return post_id;
    }

    public void setPost_id(int post_id) {
        this.post_id = post_id;
    }

    

    
    
    public Timestamp getCreate() {
        return create;
    }

    public void setCreate(Timestamp create) {
        this.create = create;
    }

    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
    
    
}
