/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import dao.ManagerDataAccess;
import entities.Comment;
import java.util.List;

/**
 *
 * @author Trong Tran
 */
public class CommentManagerBean {

    private String post_id;
    private Comment comment;
    private final ManagerDataAccess dataAccess = new ManagerDataAccess();

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }

    public boolean addComment() {
        return dataAccess.addComment(comment);
    }

    public List<Comment> getCommentsByPostID() {
        System.out.println(post_id);
        return dataAccess.getCommentsPostID(post_id);

    }

}
