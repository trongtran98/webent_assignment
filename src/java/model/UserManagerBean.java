/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import dao.ManagerDataAccess;
import entities.User;

/**
 *
 * @author Trong Tran
 */
public class UserManagerBean {
    
    private String acc;
    private User user;
    private final ManagerDataAccess dataAccess;

    public UserManagerBean() {
        this.dataAccess = new ManagerDataAccess();
    }

    public void setAcc(String acc) {
        this.acc = acc;
    }

    
    
    public void setUser(User user) {
        this.user = user;
    }

    public boolean addUser() {
        return dataAccess.addUser(user);
    }
    
    public String login(){
        return dataAccess.login(user);
    }
    
    public User getUserByAcc(){
        return dataAccess.getUserByAcc(acc);
    }
    
    public boolean updateUser(){
        return dataAccess.updateUser(user);
    }
    
    public String checkAdmin(){
        return dataAccess.checkAdmin(acc);
        
    }
}
