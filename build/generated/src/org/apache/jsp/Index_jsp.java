package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class Index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_forEach_var_items;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_if_test;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_choose;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_otherwise;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_when_test;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_c_forEach_var_items = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_c_if_test = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_c_choose = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_c_otherwise = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_c_when_test = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_c_forEach_var_items.release();
    _jspx_tagPool_c_if_test.release();
    _jspx_tagPool_c_choose.release();
    _jspx_tagPool_c_otherwise.release();
    _jspx_tagPool_c_when_test.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\" />\n");
      out.write("        <meta charset=\"utf-8\" />\n");
      out.write("        <title>Timeline - Ace Admin</title>\n");
      out.write("\n");
      out.write("        <meta name=\"description\" content=\"based on widget boxes with 2 different styles\" />\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0\" />\n");
      out.write("\n");
      out.write("        <!-- bootstrap & fontawesome -->\n");
      out.write("        <link rel=\"stylesheet\" href=\"assets/css/bootstrap.min.css\" />\n");
      out.write("        <link rel=\"stylesheet\" href=\"assets/font-awesome/4.5.0/css/font-awesome.min.css\" />\n");
      out.write("\n");
      out.write("        <!-- page specific plugin styles -->\n");
      out.write("\n");
      out.write("        <!-- text fonts -->\n");
      out.write("        <link rel=\"stylesheet\" href=\"assets/css/fonts.googleapis.com.css\" />\n");
      out.write("\n");
      out.write("        <!-- ace styles -->\n");
      out.write("        <link rel=\"stylesheet\" href=\"assets/css/ace.min.css\" class=\"ace-main-stylesheet\" id=\"main-ace-style\" />\n");
      out.write("\n");
      out.write("        <!--[if lte IE 9]>\n");
      out.write("                <link rel=\"stylesheet\" href=\"assets/css/ace-part2.min.css\" class=\"ace-main-stylesheet\" />\n");
      out.write("        <![endif]-->\n");
      out.write("        <link rel=\"stylesheet\" href=\"assets/css/ace-skins.min.css\" />\n");
      out.write("        <link rel=\"stylesheet\" href=\"assets/css/ace-rtl.min.css\" />\n");
      out.write("\n");
      out.write("        <!--[if lte IE 9]>\n");
      out.write("          <link rel=\"stylesheet\" href=\"assets/css/ace-ie.min.css\" />\n");
      out.write("        <![endif]-->\n");
      out.write("\n");
      out.write("        <!-- inline styles related to this page -->\n");
      out.write("\n");
      out.write("        <!-- ace settings handler -->\n");
      out.write("        <script src=\"assets/js/ace-extra.min.js\"></script>\n");
      out.write("\n");
      out.write("        <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->\n");
      out.write("\n");
      out.write("        <!--[if lte IE 8]>\n");
      out.write("        <script src=\"assets/js/html5shiv.min.js\"></script>\n");
      out.write("        <script src=\"assets/js/respond.min.js\"></script>\n");
      out.write("        <![endif]-->\n");
      out.write("    </head>\n");
      out.write("\n");
      out.write("    <body class=\"no-skin\">\n");
      out.write("        <div id=\"navbar\" class=\"navbar navbar-default          ace-save-state\">\n");
      out.write("            <div class=\"navbar-container ace-save-state\" id=\"navbar-container\">\n");
      out.write("                <button type=\"button\" class=\"navbar-toggle menu-toggler pull-left\" id=\"menu-toggler\" data-target=\"#sidebar\">\n");
      out.write("                    <span class=\"sr-only\">Toggle sidebar</span>\n");
      out.write("\n");
      out.write("                    <span class=\"icon-bar\"></span>\n");
      out.write("\n");
      out.write("                    <span class=\"icon-bar\"></span>\n");
      out.write("\n");
      out.write("                    <span class=\"icon-bar\"></span>\n");
      out.write("                </button>\n");
      out.write("\n");
      out.write("                <div class=\"navbar-header pull-left\">\n");
      out.write("                    <a href=\"index.html\" class=\"navbar-brand\">\n");
      out.write("                        <small>\n");
      out.write("                            <i class=\"fa fa-leaf\"></i>\n");
      out.write("                            T1608A - Fpt Aptech\n");
      out.write("                        </small>\n");
      out.write("                    </a>\n");
      out.write("                </div>\n");
      out.write("\n");
      out.write("                <div class=\"navbar-buttons navbar-header pull-right\" role=\"navigation\">\n");
      out.write("                    <ul class=\"nav ace-nav\">\n");
      out.write("                        <li class=\"grey dropdown-modal\">\n");
      out.write("                            <a data-toggle=\"dropdown\" class=\"dropdown-toggle\" href=\"#\">\n");
      out.write("                                <i class=\"ace-icon fa fa-tasks\"></i>\n");
      out.write("                                <span class=\"badge badge-grey\">4</span>\n");
      out.write("                            </a>\n");
      out.write("\n");
      out.write("                            <ul class=\"dropdown-menu-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close\">\n");
      out.write("                                <li class=\"dropdown-header\">\n");
      out.write("                                    <i class=\"ace-icon fa fa-check\"></i>\n");
      out.write("                                    4 Tasks to complete\n");
      out.write("                                </li>\n");
      out.write("\n");
      out.write("                                <li class=\"dropdown-content\">\n");
      out.write("                                    <ul class=\"dropdown-menu dropdown-navbar\">\n");
      out.write("                                        <li>\n");
      out.write("                                            <a href=\"#\">\n");
      out.write("                                                <div class=\"clearfix\">\n");
      out.write("                                                    <span class=\"pull-left\">Software Update</span>\n");
      out.write("                                                    <span class=\"pull-right\">65%</span>\n");
      out.write("                                                </div>\n");
      out.write("\n");
      out.write("                                                <div class=\"progress progress-mini\">\n");
      out.write("                                                    <div style=\"width:65%\" class=\"progress-bar\"></div>\n");
      out.write("                                                </div>\n");
      out.write("                                            </a>\n");
      out.write("                                        </li>\n");
      out.write("\n");
      out.write("                                        <li>\n");
      out.write("                                            <a href=\"#\">\n");
      out.write("                                                <div class=\"clearfix\">\n");
      out.write("                                                    <span class=\"pull-left\">Hardware Upgrade</span>\n");
      out.write("                                                    <span class=\"pull-right\">35%</span>\n");
      out.write("                                                </div>\n");
      out.write("\n");
      out.write("                                                <div class=\"progress progress-mini\">\n");
      out.write("                                                    <div style=\"width:35%\" class=\"progress-bar progress-bar-danger\"></div>\n");
      out.write("                                                </div>\n");
      out.write("                                            </a>\n");
      out.write("                                        </li>\n");
      out.write("\n");
      out.write("                                        <li>\n");
      out.write("                                            <a href=\"#\">\n");
      out.write("                                                <div class=\"clearfix\">\n");
      out.write("                                                    <span class=\"pull-left\">Unit Testing</span>\n");
      out.write("                                                    <span class=\"pull-right\">15%</span>\n");
      out.write("                                                </div>\n");
      out.write("\n");
      out.write("                                                <div class=\"progress progress-mini\">\n");
      out.write("                                                    <div style=\"width:15%\" class=\"progress-bar progress-bar-warning\"></div>\n");
      out.write("                                                </div>\n");
      out.write("                                            </a>\n");
      out.write("                                        </li>\n");
      out.write("\n");
      out.write("                                        <li>\n");
      out.write("                                            <a href=\"#\">\n");
      out.write("                                                <div class=\"clearfix\">\n");
      out.write("                                                    <span class=\"pull-left\">Bug Fixes</span>\n");
      out.write("                                                    <span class=\"pull-right\">90%</span>\n");
      out.write("                                                </div>\n");
      out.write("\n");
      out.write("                                                <div class=\"progress progress-mini progress-striped active\">\n");
      out.write("                                                    <div style=\"width:90%\" class=\"progress-bar progress-bar-success\"></div>\n");
      out.write("                                                </div>\n");
      out.write("                                            </a>\n");
      out.write("                                        </li>\n");
      out.write("                                    </ul>\n");
      out.write("                                </li>\n");
      out.write("\n");
      out.write("                                <li class=\"dropdown-footer\">\n");
      out.write("                                    <a href=\"#\">\n");
      out.write("                                        See tasks with details\n");
      out.write("                                        <i class=\"ace-icon fa fa-arrow-right\"></i>\n");
      out.write("                                    </a>\n");
      out.write("                                </li>\n");
      out.write("                            </ul>\n");
      out.write("                        </li>\n");
      out.write("\n");
      out.write("                        <li class=\"purple dropdown-modal\">\n");
      out.write("                            <a data-toggle=\"dropdown\" class=\"dropdown-toggle\" href=\"#\">\n");
      out.write("                                <i class=\"ace-icon fa fa-bell icon-animated-bell\"></i>\n");
      out.write("                                <span class=\"badge badge-important\">8</span>\n");
      out.write("                            </a>\n");
      out.write("\n");
      out.write("                            <ul class=\"dropdown-menu-right dropdown-navbar navbar-pink dropdown-menu dropdown-caret dropdown-close\">\n");
      out.write("                                <li class=\"dropdown-header\">\n");
      out.write("                                    <i class=\"ace-icon fa fa-exclamation-triangle\"></i>\n");
      out.write("                                    8 Notifications\n");
      out.write("                                </li>\n");
      out.write("\n");
      out.write("                                <li class=\"dropdown-content\">\n");
      out.write("                                    <ul class=\"dropdown-menu dropdown-navbar navbar-pink\">\n");
      out.write("                                        <li>\n");
      out.write("                                            <a href=\"#\">\n");
      out.write("                                                <div class=\"clearfix\">\n");
      out.write("                                                    <span class=\"pull-left\">\n");
      out.write("                                                        <i class=\"btn btn-xs no-hover btn-pink fa fa-comment\"></i>\n");
      out.write("                                                        New Comments\n");
      out.write("                                                    </span>\n");
      out.write("                                                    <span class=\"pull-right badge badge-info\">+12</span>\n");
      out.write("                                                </div>\n");
      out.write("                                            </a>\n");
      out.write("                                        </li>\n");
      out.write("\n");
      out.write("                                        <li>\n");
      out.write("                                            <a href=\"#\">\n");
      out.write("                                                <i class=\"btn btn-xs btn-primary fa fa-user\"></i>\n");
      out.write("                                                Bob just signed up as an editor ...\n");
      out.write("                                            </a>\n");
      out.write("                                        </li>\n");
      out.write("\n");
      out.write("                                        <li>\n");
      out.write("                                            <a href=\"#\">\n");
      out.write("                                                <div class=\"clearfix\">\n");
      out.write("                                                    <span class=\"pull-left\">\n");
      out.write("                                                        <i class=\"btn btn-xs no-hover btn-success fa fa-shopping-cart\"></i>\n");
      out.write("                                                        New Orders\n");
      out.write("                                                    </span>\n");
      out.write("                                                    <span class=\"pull-right badge badge-success\">+8</span>\n");
      out.write("                                                </div>\n");
      out.write("                                            </a>\n");
      out.write("                                        </li>\n");
      out.write("\n");
      out.write("                                        <li>\n");
      out.write("                                            <a href=\"#\">\n");
      out.write("                                                <div class=\"clearfix\">\n");
      out.write("                                                    <span class=\"pull-left\">\n");
      out.write("                                                        <i class=\"btn btn-xs no-hover btn-info fa fa-twitter\"></i>\n");
      out.write("                                                        Followers\n");
      out.write("                                                    </span>\n");
      out.write("                                                    <span class=\"pull-right badge badge-info\">+11</span>\n");
      out.write("                                                </div>\n");
      out.write("                                            </a>\n");
      out.write("                                        </li>\n");
      out.write("                                    </ul>\n");
      out.write("                                </li>\n");
      out.write("\n");
      out.write("                                <li class=\"dropdown-footer\">\n");
      out.write("                                    <a href=\"#\">\n");
      out.write("                                        See all notifications\n");
      out.write("                                        <i class=\"ace-icon fa fa-arrow-right\"></i>\n");
      out.write("                                    </a>\n");
      out.write("                                </li>\n");
      out.write("                            </ul>\n");
      out.write("                        </li>\n");
      out.write("\n");
      out.write("                        <li class=\"green dropdown-modal\">\n");
      out.write("                            <a data-toggle=\"dropdown\" class=\"dropdown-toggle\" href=\"#\">\n");
      out.write("                                <i class=\"ace-icon fa fa-envelope icon-animated-vertical\"></i>\n");
      out.write("                                <span class=\"badge badge-success\">5</span>\n");
      out.write("                            </a>\n");
      out.write("\n");
      out.write("                            <ul class=\"dropdown-menu-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close\">\n");
      out.write("                                <li class=\"dropdown-header\">\n");
      out.write("                                    <i class=\"ace-icon fa fa-envelope-o\"></i>\n");
      out.write("                                    5 Messages\n");
      out.write("                                </li>\n");
      out.write("\n");
      out.write("                                <li class=\"dropdown-content\">\n");
      out.write("                                    <ul class=\"dropdown-menu dropdown-navbar\">\n");
      out.write("                                        <li>\n");
      out.write("                                            <a href=\"#\" class=\"clearfix\">\n");
      out.write("                                                <img src=\"assets/images/avatars/avatar.png\" class=\"msg-photo\" alt=\"Alex's Avatar\" />\n");
      out.write("                                                <span class=\"msg-body\">\n");
      out.write("                                                    <span class=\"msg-title\">\n");
      out.write("                                                        <span class=\"blue\">Alex:</span>\n");
      out.write("                                                        Ciao sociis natoque penatibus et auctor ...\n");
      out.write("                                                    </span>\n");
      out.write("\n");
      out.write("                                                    <span class=\"msg-time\">\n");
      out.write("                                                        <i class=\"ace-icon fa fa-clock-o\"></i>\n");
      out.write("                                                        <span>a moment ago</span>\n");
      out.write("                                                    </span>\n");
      out.write("                                                </span>\n");
      out.write("                                            </a>\n");
      out.write("                                        </li>\n");
      out.write("\n");
      out.write("                                        <li>\n");
      out.write("                                            <a href=\"#\" class=\"clearfix\">\n");
      out.write("                                                <img src=\"assets/images/avatars/avatar3.png\" class=\"msg-photo\" alt=\"Susan's Avatar\" />\n");
      out.write("                                                <span class=\"msg-body\">\n");
      out.write("                                                    <span class=\"msg-title\">\n");
      out.write("                                                        <span class=\"blue\">Susan:</span>\n");
      out.write("                                                        Vestibulum id ligula porta felis euismod ...\n");
      out.write("                                                    </span>\n");
      out.write("\n");
      out.write("                                                    <span class=\"msg-time\">\n");
      out.write("                                                        <i class=\"ace-icon fa fa-clock-o\"></i>\n");
      out.write("                                                        <span>20 minutes ago</span>\n");
      out.write("                                                    </span>\n");
      out.write("                                                </span>\n");
      out.write("                                            </a>\n");
      out.write("                                        </li>\n");
      out.write("\n");
      out.write("                                        <li>\n");
      out.write("                                            <a href=\"#\" class=\"clearfix\">\n");
      out.write("                                                <img src=\"assets/images/avatars/avatar4.png\" class=\"msg-photo\" alt=\"Bob's Avatar\" />\n");
      out.write("                                                <span class=\"msg-body\">\n");
      out.write("                                                    <span class=\"msg-title\">\n");
      out.write("                                                        <span class=\"blue\">Bob:</span>\n");
      out.write("                                                        Nullam quis risus eget urna mollis ornare ...\n");
      out.write("                                                    </span>\n");
      out.write("\n");
      out.write("                                                    <span class=\"msg-time\">\n");
      out.write("                                                        <i class=\"ace-icon fa fa-clock-o\"></i>\n");
      out.write("                                                        <span>3:15 pm</span>\n");
      out.write("                                                    </span>\n");
      out.write("                                                </span>\n");
      out.write("                                            </a>\n");
      out.write("                                        </li>\n");
      out.write("\n");
      out.write("                                        <li>\n");
      out.write("                                            <a href=\"#\" class=\"clearfix\">\n");
      out.write("                                                <img src=\"assets/images/avatars/avatar2.png\" class=\"msg-photo\" alt=\"Kate's Avatar\" />\n");
      out.write("                                                <span class=\"msg-body\">\n");
      out.write("                                                    <span class=\"msg-title\">\n");
      out.write("                                                        <span class=\"blue\">Kate:</span>\n");
      out.write("                                                        Ciao sociis natoque eget urna mollis ornare ...\n");
      out.write("                                                    </span>\n");
      out.write("\n");
      out.write("                                                    <span class=\"msg-time\">\n");
      out.write("                                                        <i class=\"ace-icon fa fa-clock-o\"></i>\n");
      out.write("                                                        <span>1:33 pm</span>\n");
      out.write("                                                    </span>\n");
      out.write("                                                </span>\n");
      out.write("                                            </a>\n");
      out.write("                                        </li>\n");
      out.write("\n");
      out.write("                                        <li>\n");
      out.write("                                            <a href=\"#\" class=\"clearfix\">\n");
      out.write("                                                <img src=\"assets/images/avatars/avatar5.png\" class=\"msg-photo\" alt=\"Fred's Avatar\" />\n");
      out.write("                                                <span class=\"msg-body\">\n");
      out.write("                                                    <span class=\"msg-title\">\n");
      out.write("                                                        <span class=\"blue\">Fred:</span>\n");
      out.write("                                                        Vestibulum id penatibus et auctor  ...\n");
      out.write("                                                    </span>\n");
      out.write("\n");
      out.write("                                                    <span class=\"msg-time\">\n");
      out.write("                                                        <i class=\"ace-icon fa fa-clock-o\"></i>\n");
      out.write("                                                        <span>10:09 am</span>\n");
      out.write("                                                    </span>\n");
      out.write("                                                </span>\n");
      out.write("                                            </a>\n");
      out.write("                                        </li>\n");
      out.write("                                    </ul>\n");
      out.write("                                </li>\n");
      out.write("\n");
      out.write("                                <li class=\"dropdown-footer\">\n");
      out.write("                                    <a href=\"inbox.html\">\n");
      out.write("                                        See all messages\n");
      out.write("                                        <i class=\"ace-icon fa fa-arrow-right\"></i>\n");
      out.write("                                    </a>\n");
      out.write("                                </li>\n");
      out.write("                            </ul>\n");
      out.write("                        </li>\n");
      out.write("\n");
      out.write("                        <li class=\"light-blue dropdown-modal\">\n");
      out.write("                            <a data-toggle=\"dropdown\" href=\"#\" class=\"dropdown-toggle\">\n");
      out.write("                                <img class=\"nav-user-photo\" src=\"assets/images/avatars/user.jpg\" alt=\"Jason's Photo\" />\n");
      out.write("                                <span class=\"user-info\">\n");
      out.write("                                    <small>Welcome,</small>\n");
      out.write("                                    ");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${exist_acc}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\n");
      out.write("                                    ");
      if (_jspx_meth_c_if_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("                                </span>\n");
      out.write("\n");
      out.write("                                <i class=\"ace-icon fa fa-caret-down\"></i>\n");
      out.write("                            </a>\n");
      out.write("\n");
      out.write("                            <ul class=\"user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close\">\n");
      out.write("                                <li>\n");
      out.write("                                    <a href=\"#\">\n");
      out.write("                                        <i class=\"ace-icon fa fa-cog\"></i>\n");
      out.write("                                        Settings\n");
      out.write("                                    </a>\n");
      out.write("                                </li>\n");
      out.write("\n");
      out.write("                                <li>\n");
      out.write("                                    <a href=\"ViewProfileController\">\n");
      out.write("                                        <i class=\"ace-icon fa fa-user\"></i>\n");
      out.write("                                        Profile\n");
      out.write("                                    </a>\n");
      out.write("                                </li>\n");
      out.write("\n");
      out.write("                                ");
      if (_jspx_meth_c_if_1(_jspx_page_context))
        return;
      out.write("\n");
      out.write("                                <li class=\"divider\"></li>\n");
      out.write("\n");
      out.write("                                <li>\n");
      out.write("                                    <a href=\"LogOutController\">\n");
      out.write("                                        <i class=\"ace-icon fa fa-power-off\"></i>\n");
      out.write("                                        Logout\n");
      out.write("                                    </a>\n");
      out.write("                                </li>\n");
      out.write("                            </ul>\n");
      out.write("                        </li>\n");
      out.write("                    </ul>\n");
      out.write("                </div>\n");
      out.write("            </div><!-- /.navbar-container -->\n");
      out.write("        </div>\n");
      out.write("\n");
      out.write("        <div class=\"main-container ace-save-state\" id=\"main-container\">\n");
      out.write("            <script type=\"text/javascript\">\n");
      out.write("                try {\n");
      out.write("                    ace.settings.loadState('main-container')\n");
      out.write("                } catch (e) {\n");
      out.write("                }\n");
      out.write("            </script>\n");
      out.write("            <div id=\"sidebar\" class=\"sidebar                  responsive                    ace-save-state\">\n");
      out.write("                <script type=\"text/javascript\">\n");
      out.write("                    try {\n");
      out.write("                        ace.settings.loadState('sidebar')\n");
      out.write("                    } catch (e) {\n");
      out.write("                    }\n");
      out.write("                </script>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("            <div class=\"main-content\">\n");
      out.write("                <div class=\"main-content-inner\">\n");
      out.write("\n");
      out.write("\n");
      out.write("                    <div class=\"page-content\">\n");
      out.write("\n");
      out.write("\n");
      out.write("                        <div class=\"page-header\">\n");
      out.write("                            <h1>\n");
      out.write("                                Timeline\n");
      out.write("                                <small>\n");
      out.write("                                    <i class=\"ace-icon fa fa-angle-double-right\"></i>\n");
      out.write("\n");
      out.write("                                    Recent discussions\n");
      out.write("                                </small>\n");
      out.write("                            </h1>\n");
      out.write("                        </div><!-- /.page-header -->\n");
      out.write("\n");
      out.write("                        <div class=\"row\">\n");
      out.write("                            <div class=\"col-xs-12\">\n");
      out.write("                                <!-- PAGE CONTENT BEGINS -->\n");
      out.write("\n");
      out.write("\n");
      out.write("                                <div id=\"timeline-2\">\n");
      out.write("                                    <div class=\"form-group\">\n");
      out.write("                                        <label class=\"col-sm-1 control-label no-padding-right\">Your post</label>\n");
      out.write("\n");
      out.write("                                        <div class=\"col-xs-9\">\n");
      out.write("                                            <form action=\"PostController\" method=\"post\" enctype=\"multipart/form-data\">\n");
      out.write("\n");
      out.write("                                                <span class=\"input-icon\">\n");
      out.write("                                                    <input type=\"text\" name=\"new_post\" required placeholder=\"What's on your mind, ");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${exist_acc}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("?\" class=\"col-xs-10 col-sm-8\">\n");
      out.write("\n");
      out.write("                                                    <button style=\"height: 34px; padding: 0px 12px;\" class=\"btn-success btn\" type=\"submit\">Post</button>\n");
      out.write("                                                </span>\n");
      out.write("                                                <input name=\"photo\" type=\"file\">\n");
      out.write("\n");
      out.write("                                            </form>\n");
      out.write("\n");
      out.write("                                        </div>\n");
      out.write("                                    </div>\n");
      out.write("                                    <div class=\"row\">\n");
      out.write("                                        <div class=\"col-xs-12 col-sm-10 col-sm-offset-1\">\n");
      out.write("\n");
      out.write("                                            ");
      model.UserManagerBean checkAdminBean = null;
      synchronized (_jspx_page_context) {
        checkAdminBean = (model.UserManagerBean) _jspx_page_context.getAttribute("checkAdminBean", PageContext.PAGE_SCOPE);
        if (checkAdminBean == null){
          checkAdminBean = new model.UserManagerBean();
          _jspx_page_context.setAttribute("checkAdminBean", checkAdminBean, PageContext.PAGE_SCOPE);
        }
      }
      out.write("\n");
      out.write("\n");
      out.write("                                            ");
      if (_jspx_meth_c_forEach_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("                                        </div>\n");
      out.write("                                    </div>\n");
      out.write("                                </div>\n");
      out.write("\n");
      out.write("                                <!-- PAGE CONTENT ENDS -->\n");
      out.write("                            </div><!-- /.col -->\n");
      out.write("                        </div><!-- /.row -->\n");
      out.write("                    </div><!-- /.page-content -->\n");
      out.write("                </div>\n");
      out.write("            </div><!-- /.main-content -->\n");
      out.write("\n");
      out.write("            <div class=\"footer\">\n");
      out.write("                <div class=\"footer-inner\">\n");
      out.write("                    <div class=\"footer-content\">\n");
      out.write("                        <span class=\"bigger-120\">\n");
      out.write("                            <span class=\"blue bolder\">Ace</span>\n");
      out.write("                            Application &copy; 2013-2014\n");
      out.write("                        </span>\n");
      out.write("\n");
      out.write("                        &nbsp; &nbsp;\n");
      out.write("                        <span class=\"action-buttons\">\n");
      out.write("                            <a href=\"#\">\n");
      out.write("                                <i class=\"ace-icon fa fa-twitter-square light-blue bigger-150\"></i>\n");
      out.write("                            </a>\n");
      out.write("\n");
      out.write("                            <a href=\"#\">\n");
      out.write("                                <i class=\"ace-icon fa fa-facebook-square text-primary bigger-150\"></i>\n");
      out.write("                            </a>\n");
      out.write("\n");
      out.write("                            <a href=\"#\">\n");
      out.write("                                <i class=\"ace-icon fa fa-rss-square orange bigger-150\"></i>\n");
      out.write("                            </a>\n");
      out.write("                        </span>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("            <a href=\"#\" id=\"btn-scroll-up\" class=\"btn-scroll-up btn btn-sm btn-inverse\">\n");
      out.write("                <i class=\"ace-icon fa fa-angle-double-up icon-only bigger-110\"></i>\n");
      out.write("            </a>\n");
      out.write("        </div><!-- /.main-container -->\n");
      out.write("\n");
      out.write("        <!-- basic scripts -->\n");
      out.write("\n");
      out.write("        <!--[if !IE]> -->\n");
      out.write("        <script src=\"assets/js/jquery-2.1.4.min.js\"></script>\n");
      out.write("\n");
      out.write("        <!-- <![endif]-->\n");
      out.write("\n");
      out.write("        <!--[if IE]>\n");
      out.write("    <script src=\"assets/js/jquery-1.11.3.min.js\"></script>\n");
      out.write("    <![endif]-->\n");
      out.write("        <script type=\"text/javascript\">\n");
      out.write("                        if ('ontouchstart' in document.documentElement)\n");
      out.write("                            document.write(\"<script src='assets/js/jquery.mobile.custom.min.js'>\" + \"<\" + \"/script>\");\n");
      out.write("        </script>\n");
      out.write("        <script src=\"assets/js/bootstrap.min.js\"></script>\n");
      out.write("\n");
      out.write("        <!-- page specific plugin scripts -->\n");
      out.write("\n");
      out.write("        <!-- ace scripts -->\n");
      out.write("        <script src=\"assets/js/ace-elements.min.js\"></script>\n");
      out.write("        <script src=\"assets/js/ace.min.js\"></script>\n");
      out.write("\n");
      out.write("        <!-- inline scripts related to this page -->\n");
      out.write("\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_c_if_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_0 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_0.setPageContext(_jspx_page_context);
    _jspx_th_c_if_0.setParent(null);
    _jspx_th_c_if_0.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${is_admin eq true}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_0 = _jspx_th_c_if_0.doStartTag();
    if (_jspx_eval_c_if_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\n");
        out.write("                                        (ADMIN)\n");
        out.write("                                    ");
        int evalDoAfterBody = _jspx_th_c_if_0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_0);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_0);
    return false;
  }

  private boolean _jspx_meth_c_if_1(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_1 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_1.setPageContext(_jspx_page_context);
    _jspx_th_c_if_1.setParent(null);
    _jspx_th_c_if_1.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${is_admin eq true}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_1 = _jspx_th_c_if_1.doStartTag();
    if (_jspx_eval_c_if_1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\n");
        out.write("                                    <li>\n");
        out.write("                                        <a href=\"AddUser.jsp\">\n");
        out.write("                                            <i class=\"ace-icon fa fa-user\"></i>\n");
        out.write("                                            Create user\n");
        out.write("                                        </a>\n");
        out.write("                                    </li>\n");
        out.write("                                ");
        int evalDoAfterBody = _jspx_th_c_if_1.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_1);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_1);
    return false;
  }

  private boolean _jspx_meth_c_forEach_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    HttpSession session = _jspx_page_context.getSession();
    ServletContext application = _jspx_page_context.getServletContext();
    HttpServletRequest request = (HttpServletRequest)_jspx_page_context.getRequest();
    //  c:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_forEach_0 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _jspx_tagPool_c_forEach_var_items.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_c_forEach_0.setPageContext(_jspx_page_context);
    _jspx_th_c_forEach_0.setParent(null);
    _jspx_th_c_forEach_0.setItems((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${posts}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    _jspx_th_c_forEach_0.setVar("post");
    int[] _jspx_push_body_count_c_forEach_0 = new int[] { 0 };
    try {
      int _jspx_eval_c_forEach_0 = _jspx_th_c_forEach_0.doStartTag();
      if (_jspx_eval_c_forEach_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\n");
          out.write("                                                <div class=\"timeline-container timeline-style2\">\n");
          out.write("                                                    <span class=\"timeline-label\">\n");
          out.write("                                                        <b>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${post.create.toLocaleString()}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</b>\n");
          out.write("                                                    </span>\n");
          out.write("\n");
          out.write("                                                    <div class=\"timeline-item clearfix\">\n");
          out.write("                                                        <div class=\"timeline-info\">\n");
          out.write("\n");
          out.write("                                                            ");
          org.apache.jasper.runtime.JspRuntimeLibrary.handleSetPropertyExpression(_jspx_page_context.findAttribute("checkAdminBean"), "acc", "${post.user_account}", _jspx_page_context, null);
          out.write("\n");
          out.write("                                                            <span class=\"timeline-date\">\n");
          out.write("                                                                ");
          if (_jspx_meth_c_choose_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_forEach_0, _jspx_page_context, _jspx_push_body_count_c_forEach_0))
            return true;
          out.write("\n");
          out.write("\n");
          out.write("\n");
          out.write("                                                            </span>\n");
          out.write("\n");
          out.write("\n");
          out.write("\n");
          out.write("                                                            <i style=\"background-color: #6FB3E0!important;\" class=\"timeline-indicator btn btn-info no-hover\"></i>\n");
          out.write("                                                        </div>\n");
          out.write("\n");
          out.write("                                                        <div class=\"widget-box transparent\">\n");
          out.write("                                                            <div class=\"widget-body\">\n");
          out.write("                                                                <div class=\"widget-main no-padding\">  \n");
          out.write("                                                                    <p style=\"font-size: 16px;\"><b style=\"color: #002a80;\"></b>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${post.content}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</p>\n");
          out.write("                                                                        ");
          if (_jspx_meth_c_if_2((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_forEach_0, _jspx_page_context, _jspx_push_body_count_c_forEach_0))
            return true;
          out.write("\n");
          out.write("                                                                </div>\n");
          out.write("                                                            </div>\n");
          out.write("                                                        </div>\n");
          out.write("                                                    </div>\n");
          out.write("                                                    <div class=\"timeline-items\">\n");
          out.write("\n");
          out.write("                                                        ");
          model.CommentManagerBean commentBean = null;
          synchronized (request) {
            commentBean = (model.CommentManagerBean) _jspx_page_context.getAttribute("commentBean", PageContext.REQUEST_SCOPE);
            if (commentBean == null){
              commentBean = new model.CommentManagerBean();
              _jspx_page_context.setAttribute("commentBean", commentBean, PageContext.REQUEST_SCOPE);
            }
          }
          out.write("\n");
          out.write("                                                        ");
          org.apache.jasper.runtime.JspRuntimeLibrary.handleSetPropertyExpression(_jspx_page_context.findAttribute("commentBean"), "post_id", "${post.id}", _jspx_page_context, null);
          out.write("  \n");
          out.write("\n");
          out.write("                                                        ");
          if (_jspx_meth_c_forEach_1((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_forEach_0, _jspx_page_context, _jspx_push_body_count_c_forEach_0))
            return true;
          out.write("\n");
          out.write("                                                        <div class=\"timeline-item clearfix\">\n");
          out.write("                                                            <div class=\"timeline-info\">\n");
          out.write("                                                                <span class=\"timeline-date\">now</span>\n");
          out.write("\n");
          out.write("                                                                <i class=\"timeline-indicator btn btn-info no-hover\"></i>\n");
          out.write("                                                            </div>\n");
          out.write("\n");
          out.write("                                                            <div class=\"widget-box transparent\">\n");
          out.write("                                                                <div class=\"widget-body\">\n");
          out.write("                                                                    <div class=\"widget-main no-padding\">\n");
          out.write("                                                                        <form action=\"CommentController\" method=\"post\" enctype=\"multipart/form-data\">\n");
          out.write("\n");
          out.write("                                                                            <span class=\"input-icon\">\n");
          out.write("                                                                                <input name=\"post_id\" value=\"");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${post.id}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("\" hidden/>\n");
          out.write("\n");
          out.write("                                                                                <input required type=\"text\" name=\"new_comment\" placeholder=\"Say something about this post!\" class=\"col-xs-5\">\n");
          out.write("                                                                                <button style=\"height: 34px;\" class=\"btn-primary\" type=\"submit\">Send</button>\n");
          out.write("                                                                            </span>\n");
          out.write("                                                                            <input name=\"photo\" type=\"file\">\n");
          out.write("\n");
          out.write("                                                                        </form>\n");
          out.write("                                                                    </div>\n");
          out.write("                                                                </div>\n");
          out.write("                                                            </div>\n");
          out.write("                                                        </div>   \n");
          out.write("                                                    </div><!-- /.timeline-items -->\n");
          out.write("                                                </div><!-- /.timeline-container -->\n");
          out.write("                                            ");
          int evalDoAfterBody = _jspx_th_c_forEach_0.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_forEach_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_c_forEach_0[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_c_forEach_0.doCatch(_jspx_exception);
    } finally {
      _jspx_th_c_forEach_0.doFinally();
      _jspx_tagPool_c_forEach_var_items.reuse(_jspx_th_c_forEach_0);
    }
    return false;
  }

  private boolean _jspx_meth_c_choose_0(javax.servlet.jsp.tagext.JspTag _jspx_th_c_forEach_0, PageContext _jspx_page_context, int[] _jspx_push_body_count_c_forEach_0)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:choose
    org.apache.taglibs.standard.tag.common.core.ChooseTag _jspx_th_c_choose_0 = (org.apache.taglibs.standard.tag.common.core.ChooseTag) _jspx_tagPool_c_choose.get(org.apache.taglibs.standard.tag.common.core.ChooseTag.class);
    _jspx_th_c_choose_0.setPageContext(_jspx_page_context);
    _jspx_th_c_choose_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_forEach_0);
    int _jspx_eval_c_choose_0 = _jspx_th_c_choose_0.doStartTag();
    if (_jspx_eval_c_choose_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\n");
        out.write("                                                                    ");
        if (_jspx_meth_c_when_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_choose_0, _jspx_page_context, _jspx_push_body_count_c_forEach_0))
          return true;
        out.write("\n");
        out.write("                                                                    ");
        if (_jspx_meth_c_otherwise_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_choose_0, _jspx_page_context, _jspx_push_body_count_c_forEach_0))
          return true;
        out.write("\n");
        out.write("                                                                ");
        int evalDoAfterBody = _jspx_th_c_choose_0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_choose_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_choose.reuse(_jspx_th_c_choose_0);
      return true;
    }
    _jspx_tagPool_c_choose.reuse(_jspx_th_c_choose_0);
    return false;
  }

  private boolean _jspx_meth_c_when_0(javax.servlet.jsp.tagext.JspTag _jspx_th_c_choose_0, PageContext _jspx_page_context, int[] _jspx_push_body_count_c_forEach_0)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:when
    org.apache.taglibs.standard.tag.rt.core.WhenTag _jspx_th_c_when_0 = (org.apache.taglibs.standard.tag.rt.core.WhenTag) _jspx_tagPool_c_when_test.get(org.apache.taglibs.standard.tag.rt.core.WhenTag.class);
    _jspx_th_c_when_0.setPageContext(_jspx_page_context);
    _jspx_th_c_when_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_choose_0);
    _jspx_th_c_when_0.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${checkAdminBean.checkAdmin() eq 'ADMIN'}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_when_0 = _jspx_th_c_when_0.doStartTag();
    if (_jspx_eval_c_when_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\n");
        out.write("                                                                        <b style=\"color: red;\">\n");
        out.write("                                                                            ");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${post.user_account}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write("\n");
        out.write("                                                                            (ADMIN)\n");
        out.write("                                                                        </b>\n");
        out.write("                                                                    ");
        int evalDoAfterBody = _jspx_th_c_when_0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_when_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_when_test.reuse(_jspx_th_c_when_0);
      return true;
    }
    _jspx_tagPool_c_when_test.reuse(_jspx_th_c_when_0);
    return false;
  }

  private boolean _jspx_meth_c_otherwise_0(javax.servlet.jsp.tagext.JspTag _jspx_th_c_choose_0, PageContext _jspx_page_context, int[] _jspx_push_body_count_c_forEach_0)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:otherwise
    org.apache.taglibs.standard.tag.common.core.OtherwiseTag _jspx_th_c_otherwise_0 = (org.apache.taglibs.standard.tag.common.core.OtherwiseTag) _jspx_tagPool_c_otherwise.get(org.apache.taglibs.standard.tag.common.core.OtherwiseTag.class);
    _jspx_th_c_otherwise_0.setPageContext(_jspx_page_context);
    _jspx_th_c_otherwise_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_choose_0);
    int _jspx_eval_c_otherwise_0 = _jspx_th_c_otherwise_0.doStartTag();
    if (_jspx_eval_c_otherwise_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\n");
        out.write("                                                                        <b style=\"color: #002a80;\">\n");
        out.write("                                                                            ");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${post.user_account}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write("\n");
        out.write("\n");
        out.write("                                                                        </b>\n");
        out.write("                                                                    ");
        int evalDoAfterBody = _jspx_th_c_otherwise_0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_otherwise_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_otherwise.reuse(_jspx_th_c_otherwise_0);
      return true;
    }
    _jspx_tagPool_c_otherwise.reuse(_jspx_th_c_otherwise_0);
    return false;
  }

  private boolean _jspx_meth_c_if_2(javax.servlet.jsp.tagext.JspTag _jspx_th_c_forEach_0, PageContext _jspx_page_context, int[] _jspx_push_body_count_c_forEach_0)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_2 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_2.setPageContext(_jspx_page_context);
    _jspx_th_c_if_2.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_forEach_0);
    _jspx_th_c_if_2.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${not empty post.picture}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_2 = _jspx_th_c_if_2.doStartTag();
    if (_jspx_eval_c_if_2 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\n");
        out.write("                                                                        <img src=\"images/posts/");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${post.picture}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write("\" height=\"170\" width=\"230\"/>\n");
        out.write("                                                                    ");
        int evalDoAfterBody = _jspx_th_c_if_2.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_2);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_2);
    return false;
  }

  private boolean _jspx_meth_c_forEach_1(javax.servlet.jsp.tagext.JspTag _jspx_th_c_forEach_0, PageContext _jspx_page_context, int[] _jspx_push_body_count_c_forEach_0)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    HttpServletRequest request = (HttpServletRequest)_jspx_page_context.getRequest();
    //  c:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_forEach_1 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _jspx_tagPool_c_forEach_var_items.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_c_forEach_1.setPageContext(_jspx_page_context);
    _jspx_th_c_forEach_1.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_forEach_0);
    _jspx_th_c_forEach_1.setVar("comment");
    _jspx_th_c_forEach_1.setItems((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${commentBean.commentsByPostID}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    int[] _jspx_push_body_count_c_forEach_1 = new int[] { 0 };
    try {
      int _jspx_eval_c_forEach_1 = _jspx_th_c_forEach_1.doStartTag();
      if (_jspx_eval_c_forEach_1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\n");
          out.write("                                                            <div class=\"timeline-item clearfix\">\n");
          out.write("                                                                <div class=\"timeline-info\">\n");
          out.write("                                                                    <span class=\"timeline-date\">");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${comment.create.toLocaleString()}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</span>\n");
          out.write("\n");
          out.write("                                                                    <i class=\"timeline-indicator btn btn-info no-hover\"></i>\n");
          out.write("                                                                </div>\n");
          out.write("\n");
          out.write("                                                                <div class=\"widget-box transparent\">\n");
          out.write("                                                                    <div class=\"widget-body\">\n");
          out.write("                                                                        <div class=\"widget-main no-padding\">\n");
          out.write("                                                                            <p>\n");
          out.write("                                                                                ");
          org.apache.jasper.runtime.JspRuntimeLibrary.handleSetPropertyExpression(_jspx_page_context.findAttribute("checkAdminBean"), "acc", "${comment.user_account}", _jspx_page_context, null);
          out.write("\n");
          out.write("                                                                                ");
          if (_jspx_meth_c_choose_1((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_forEach_1, _jspx_page_context, _jspx_push_body_count_c_forEach_1))
            return true;
          out.write("\n");
          out.write("                                                                                :&nbsp;");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${comment.content}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("\n");
          out.write("                                                                            </p>\n");
          out.write("                                                                            ");
          if (_jspx_meth_c_if_3((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_forEach_1, _jspx_page_context, _jspx_push_body_count_c_forEach_1))
            return true;
          out.write("\n");
          out.write("                                                                        </div>\n");
          out.write("                                                                    </div>\n");
          out.write("                                                                </div>\n");
          out.write("                                                            </div>\n");
          out.write("\n");
          out.write("                                                        ");
          int evalDoAfterBody = _jspx_th_c_forEach_1.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_forEach_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_c_forEach_1[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_c_forEach_1.doCatch(_jspx_exception);
    } finally {
      _jspx_th_c_forEach_1.doFinally();
      _jspx_tagPool_c_forEach_var_items.reuse(_jspx_th_c_forEach_1);
    }
    return false;
  }

  private boolean _jspx_meth_c_choose_1(javax.servlet.jsp.tagext.JspTag _jspx_th_c_forEach_1, PageContext _jspx_page_context, int[] _jspx_push_body_count_c_forEach_1)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:choose
    org.apache.taglibs.standard.tag.common.core.ChooseTag _jspx_th_c_choose_1 = (org.apache.taglibs.standard.tag.common.core.ChooseTag) _jspx_tagPool_c_choose.get(org.apache.taglibs.standard.tag.common.core.ChooseTag.class);
    _jspx_th_c_choose_1.setPageContext(_jspx_page_context);
    _jspx_th_c_choose_1.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_forEach_1);
    int _jspx_eval_c_choose_1 = _jspx_th_c_choose_1.doStartTag();
    if (_jspx_eval_c_choose_1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\n");
        out.write("                                                                                    ");
        if (_jspx_meth_c_when_1((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_choose_1, _jspx_page_context, _jspx_push_body_count_c_forEach_1))
          return true;
        out.write("\n");
        out.write("                                                                                    ");
        if (_jspx_meth_c_otherwise_1((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_choose_1, _jspx_page_context, _jspx_push_body_count_c_forEach_1))
          return true;
        out.write("\n");
        out.write("                                                                                ");
        int evalDoAfterBody = _jspx_th_c_choose_1.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_choose_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_choose.reuse(_jspx_th_c_choose_1);
      return true;
    }
    _jspx_tagPool_c_choose.reuse(_jspx_th_c_choose_1);
    return false;
  }

  private boolean _jspx_meth_c_when_1(javax.servlet.jsp.tagext.JspTag _jspx_th_c_choose_1, PageContext _jspx_page_context, int[] _jspx_push_body_count_c_forEach_1)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:when
    org.apache.taglibs.standard.tag.rt.core.WhenTag _jspx_th_c_when_1 = (org.apache.taglibs.standard.tag.rt.core.WhenTag) _jspx_tagPool_c_when_test.get(org.apache.taglibs.standard.tag.rt.core.WhenTag.class);
    _jspx_th_c_when_1.setPageContext(_jspx_page_context);
    _jspx_th_c_when_1.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_choose_1);
    _jspx_th_c_when_1.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${checkAdminBean.checkAdmin() eq 'ADMIN'}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_when_1 = _jspx_th_c_when_1.doStartTag();
    if (_jspx_eval_c_when_1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\n");
        out.write("                                                                                        <b style=\"color: red;\">\n");
        out.write("                                                                                            ");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${comment.user_account}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write("\n");
        out.write("                                                                                            (ADMIN)\n");
        out.write("                                                                                        </b>\n");
        out.write("                                                                                    ");
        int evalDoAfterBody = _jspx_th_c_when_1.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_when_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_when_test.reuse(_jspx_th_c_when_1);
      return true;
    }
    _jspx_tagPool_c_when_test.reuse(_jspx_th_c_when_1);
    return false;
  }

  private boolean _jspx_meth_c_otherwise_1(javax.servlet.jsp.tagext.JspTag _jspx_th_c_choose_1, PageContext _jspx_page_context, int[] _jspx_push_body_count_c_forEach_1)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:otherwise
    org.apache.taglibs.standard.tag.common.core.OtherwiseTag _jspx_th_c_otherwise_1 = (org.apache.taglibs.standard.tag.common.core.OtherwiseTag) _jspx_tagPool_c_otherwise.get(org.apache.taglibs.standard.tag.common.core.OtherwiseTag.class);
    _jspx_th_c_otherwise_1.setPageContext(_jspx_page_context);
    _jspx_th_c_otherwise_1.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_choose_1);
    int _jspx_eval_c_otherwise_1 = _jspx_th_c_otherwise_1.doStartTag();
    if (_jspx_eval_c_otherwise_1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\n");
        out.write("                                                                                        <b style=\"color: #002a80;\">\n");
        out.write("                                                                                            ");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${comment.user_account}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write("\n");
        out.write("                                                                                        </b>\n");
        out.write("                                                                                    ");
        int evalDoAfterBody = _jspx_th_c_otherwise_1.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_otherwise_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_otherwise.reuse(_jspx_th_c_otherwise_1);
      return true;
    }
    _jspx_tagPool_c_otherwise.reuse(_jspx_th_c_otherwise_1);
    return false;
  }

  private boolean _jspx_meth_c_if_3(javax.servlet.jsp.tagext.JspTag _jspx_th_c_forEach_1, PageContext _jspx_page_context, int[] _jspx_push_body_count_c_forEach_1)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_3 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_3.setPageContext(_jspx_page_context);
    _jspx_th_c_if_3.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_forEach_1);
    _jspx_th_c_if_3.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${not empty comment.picture}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_3 = _jspx_th_c_if_3.doStartTag();
    if (_jspx_eval_c_if_3 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\n");
        out.write("                                                                                <img src=\"images/comments/");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${comment.picture}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write("\" height=\"140\" width=\"180\"/>\n");
        out.write("                                                                            ");
        int evalDoAfterBody = _jspx_th_c_if_3.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_3.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_3);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_3);
    return false;
  }
}
